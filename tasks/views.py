from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from tasks.models import Task
from django.contrib.auth.decorators import login_required

# Create your views here.


@login_required
def list_tasks(request):
    user_tasks = Task.objects.filter(assignee=request.user)

    context = {"user_tasks": user_tasks}
    return render(request, "tasks/list_tasks.html", context)


@login_required
def create_task(request):
    if request.method == "POST":
        task_form = TaskForm(request.POST)
        if task_form.is_valid():
            task_form.save()
            return redirect("list_projects")
    else:
        task_form = TaskForm()
    context = {
        "task_form": task_form,
    }

    return render(request, "tasks/create_task.html", context)
